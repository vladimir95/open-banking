# CONTRIBUTING

## Looking for an issue

If you are interesting in contribution - look for an issue with `lfm` label and without an assignee in it.
If all issues with `lfm` label assigned, look for issues without an activity (abandoned). If assignee didn't wrote or commit anything in more than one week - feel free to reassign issue to you.
Always assign issue to you before the start, also better to write comment about your plans.


## Submitting changes

To contribute your change please submit a merge request (MR) into the master branch. 
After that - project maintainers would review and approve MR.
Every MR should contain link to the issue and automatic `/close` hint - for more information, see [Managing Issues](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically).
